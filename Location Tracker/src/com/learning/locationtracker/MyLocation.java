package com.learning.locationtracker;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class MyLocation {

	protected static final String TAG = "MyLocation";
	Timer mTimeoutTimer;
    LocationManager mLocationManager;
    LocationResult mLocationResult;
    boolean gpsEnabled=false;
    boolean networkEnabled=false;

    public boolean getLocation(Context context, LocationResult result)
    {
        //I use mLocationResult callback class to pass location value from MyLocation to user code.
        mLocationResult=result;
        if(mLocationManager==null)
            mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //exceptions will be thrown if provider is not permitted.
        try{gpsEnabled=mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
        try{networkEnabled=mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}

        //don't start listeners if no provider is enabled
        if(!gpsEnabled && !networkEnabled)
            return false;

        if(gpsEnabled)
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
        if(networkEnabled)
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        mTimeoutTimer=new Timer();
        mTimeoutTimer.schedule(new GetLastLocation(), 20000);
        return true;
    }

    LocationListener locationListenerGps = new LocationListener() {
    	
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
		@Override
		public void onLocationChanged(Location location) {
			mTimeoutTimer.cancel();
            mLocationResult.gotLocation(location);
            mLocationManager.removeUpdates(this);
            mLocationManager.removeUpdates(locationListenerNetwork);
            
            Log.i(TAG, "Latitude: " + location.getLatitude() );
        	Log.i(TAG, "Longitude: " + location.getLongitude() );
        	Log.i(TAG, "Altitude: " + location.getAltitude() );
		}
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            mTimeoutTimer.cancel();
            mLocationResult.gotLocation(location);
            mLocationManager.removeUpdates(this);
            mLocationManager.removeUpdates(locationListenerGps);
            
            Log.i(TAG, "Latitude: " + location.getLatitude() );
        	Log.i(TAG, "Longitude: " + location.getLongitude() );
        	Log.i(TAG, "Altitude: " + location.getAltitude() );
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    class GetLastLocation extends TimerTask {
        @Override
        public void run() {
             mLocationManager.removeUpdates(locationListenerGps);
             mLocationManager.removeUpdates(locationListenerNetwork);

             Location net_loc=null, gps_loc=null;
             if(gpsEnabled)
                 gps_loc=mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
             else
            	 if(networkEnabled)
            		 net_loc=mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

             //if there are both values use the latest one
             if(gps_loc!=null && net_loc!=null){
                 if(gps_loc.getTime()>net_loc.getTime())
                     mLocationResult.gotLocation(gps_loc);
                 else
                	 mLocationResult.gotLocation(net_loc);
                 return;
             }

             if(gps_loc!=null){
            	 mLocationResult.gotLocation(gps_loc);
                 return;
             }
             if(net_loc!=null){
            	 mLocationResult.gotLocation(net_loc);
                 return;
             }
             
             mLocationResult.gotLocation(null);
        }
    }

    public static abstract class LocationResult{
        public abstract void gotLocation(Location location);
    }
	
}
