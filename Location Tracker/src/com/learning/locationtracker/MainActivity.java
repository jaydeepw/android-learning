package com.learning.locationtracker;

import com.learning.locationtracker.MyLocation.LocationResult;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {
	
	private static final String TAG = "MainActivity";
	private LocationListener mLocationListener;
	private LocationManager mLocationManager;
	private LocationResult mLocationResult;
	
	private Handler mHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // Acquire a reference to the system Location Manager
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        /*mLocationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
              // Called when a new location is found by the network location provider.
              //makeUseOfNewLocation(location);
            	Log.i(TAG, "Latitude: " + location.getLatitude() );
            	Log.i(TAG, "Longitude: " + location.getLongitude() );
            	Log.i(TAG, "Altitude: " + location.getAltitude() );
            	updateUI( location.getLatitude(), location.getLongitude(), location.getAltitude() );
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            	
            }

            public void onProviderEnabled(String provider) {
            	
            }

            public void onProviderDisabled(String provider) {
            	
            }
          };*/
          
          
          mLocationResult = new LocationResult(){
        	    @Override
        	    public void gotLocation(Location location){
        	    	
        	    	if(location != null){
        	    		Message msg = new Message();
        	    		msg.what =1;
        	    		msg.obj = location;
        	    		mHandler.sendMessage(msg);
        	    	}
        	    }
        	};
        	MyLocation myLocation = new MyLocation();
        	myLocation.getLocation(this, mLocationResult);
          
          //requestLocationUpdates();
          
          initExplicitRequestLocationUpdate();
          
          mHandler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					
					Location location = (Location) msg.obj;
					updateUI( location.getLatitude(), location.getLongitude(), location.getAltitude() );
					break;

				default:
					break;
				}
				return false;
			}
		});
        
    }

    private void initExplicitRequestLocationUpdate() {
    	
    	Button requestUpdateButton = (Button) findViewById(R.id.location_request_update);
    	requestUpdateButton.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//requestLocationUpdates();
				MyLocation myLocation = new MyLocation();
	        	myLocation.getLocation(MainActivity.this, mLocationResult);
			}
		});
		
	}

	private void requestLocationUpdates() {
        // Register the listener with the Location Manager to receive location updates
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, mLocationListener);
	}

	protected void updateUI(double latitude, double longitude, double altitude) {
		TextView locationTv = null;
		
		locationTv = (TextView) findViewById(R.id.location_latitude);
		locationTv.setText("" + latitude);
		
		locationTv = (TextView) findViewById(R.id.location_longitude);
		locationTv.setText("" + longitude);
		
		locationTv = (TextView) findViewById(R.id.location_altitude);
		locationTv.setText("" + altitude);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
