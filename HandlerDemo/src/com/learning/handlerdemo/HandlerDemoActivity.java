package com.learning.handlerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HandlerDemoActivity extends Activity {
	
	private final String TAG = "HandlerDemoActivity";
	private Handler mHandler;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mHandler = new Handler(new Handler.Callback() {
			
			@Override
			public boolean handleMessage(Message msg) {
				
				
				TextView tv = (TextView) findViewById(R.id.counter);
				tv.setText("");
				tv.setText("msg.what: " + msg.what);
				Log.i(TAG, "msg.what: " + msg.what);
				
				return true;
			}
		});
        
        
        Button counterButton = (Button) findViewById(R.id.counterButton);
        counterButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startCounter();
			}
		});
    }
    
    
    private void startCounter() {
		Thread counterThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				int i =0;
				
				while(i < 10){
					
					try {
						Thread.currentThread().sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
						Log.e(TAG, "Error while sleeping the thread.");
					}
					
					
					Message msg = new Message();
					msg.what = i;
					
					mHandler.sendMessage(msg);
					
					i++;
				}// end while
				
				
				
			}
		});
		
		
		counterThread.start();
	}
    
    
} 