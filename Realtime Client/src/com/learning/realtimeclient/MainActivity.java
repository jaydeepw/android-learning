package com.learning.realtimeclient;

import java.io.IOException;

import org.apache.http.ParseException;
import org.apache.http.auth.AuthenticationException;
import org.json.JSONException;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {
	
	private static final String TAG = "MainActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        initApiCallButton();
    }

	private void initApiCallButton() {
		Button apiCallButton = (Button) findViewById(R.id.call_api);
		apiCallButton.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.i(TAG, "#onClick the API call button has been clicked.");
				
				ServerConnectionManager serConnMgr = new ServerConnectionManager();
				
				
				try {
					serConnMgr.track();
				} catch (AuthenticationException e) {
					e.printStackTrace();
					Log.i(TAG, "" + e.getMessage() );
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
    
}
