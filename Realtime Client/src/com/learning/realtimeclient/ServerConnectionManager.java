package com.learning.realtimeclient;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.http.HttpConnection;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;


public class ServerConnectionManager {
	
	private String TAG = "ServerConnectionManager";

	private static ServerConnectionManager connManager;
	private HttpClient mHttpClient;
	private Context mAppContext = null;
	

	private String USER_AGENT = "Android-"+android.os.Build.VERSION.RELEASE+"/IntouchId/";
	private final int REGISTRATION_TIMEOUT = 70 * 1000; // 70 sec 
	
	/**
	 * Create Http Client
	 */
	public ServerConnectionManager() {
		if (mHttpClient == null) {
            mHttpClient = new DefaultHttpClient();
            final HttpParams params = mHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params,REGISTRATION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, REGISTRATION_TIMEOUT);
            ConnManagerParams.setTimeout(params, REGISTRATION_TIMEOUT);
        }
	}

	public static ServerConnectionManager getInstance() {
		if (connManager == null) {
			connManager = new ServerConnectionManager();
		}
		return connManager;
	}
	
	
	/**
	 * call this method for the first time when u create a connection to the server
	 * Otherwise just call, getInstance static method 
	 * @param context
	 * @return ServerConnectionManager
	 */
	/*public static ServerConnectionManager createInstance(String appVersionName) {
		connManager = new ServerConnectionManager(appVersionName);
		return connManager;
	}*/
	
	/**
	 * Set the application context
	 * 
	 * @param context
	 */
	public void setApplicationContext(Context context)
	{
		mAppContext = context;
	}
    
    public boolean track() throws JSONException, ParseException, IOException, AuthenticationException {
        final String TAG = getClass().getName() + "loginUser";
    	
        String apiUrl  = "http://realtime-intouchid.rhcloud.com:80/api/v1/track/";
        //String apiUrl  = "http://192.168.0.101:8080/api/v1/track/";
        //String uNamePass = username + ":" + password;
        HttpResponse resp = null;

    	Log.i(TAG, "apiUrl: " + apiUrl);
    	
    	HttpGet httpGet = new HttpGet(apiUrl);
    	//httpGet.setHeader("Accept", "application/json");
    	//httpGet.setHeader("Host", "api.nodester.com");
    	//httpGet.setHeader("Authorization", "Basic " + Base64.encode(new String(uNamePass).getBytes()));
    	//httpGet.setHeader("Content-Type", "application/json");
		
        try {
        	resp = mHttpClient.execute(httpGet);
		} catch (ClientProtocolException e1) {
			Log.e(TAG, "Error authenticating user Reason : " + e1.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e1.getMessage());
		} catch (IOException e2) {
			Log.e(TAG, "Error authenticating user Reason : " + e2.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e2.getMessage());
        }

        Log.i(TAG, "resp: " + resp);
        
        if(resp == null){
        	Log.e(TAG, "Server failed to respong. Response is null");
        	return false;
        }
        
        if ( resp != null){
        	final String response = EntityUtils.toString(resp.getEntity());
        	Log.i(TAG, " resp: " + response);
        	
        	int httpStatus = resp.getStatusLine().getStatusCode();
        	
        	if ( httpStatus == HttpStatus.SC_OK || httpStatus == HttpStatus.SC_CREATED){
        		Log.i(TAG, "Login successful");
        		return true;	
        	}
        	
        	if ( httpStatus == HttpStatus.SC_UNAUTHORIZED ){
        		Log.e(TAG, "Login failed");
        		return false;
        	}
        }
        
        return false;
	}/*
    
    public boolean getApps(String username, String password, HashMap<String, JSONArray> outputData) throws JSONException, ParseException, IOException, AuthenticationException {
        final String TAG = getClass().getName() + "getApps";
    	
        String apiUrl  = Constants.API_URL_APPS;
        String uNamePass = username + ":" + password;
        HttpResponse resp = null;

    	Log.i(TAG, "apiUrl: " + apiUrl);
    	
    	HttpGet httpGet = new HttpGet(apiUrl);
    	httpGet.setHeader("Accept", "application/json");
    	httpGet.setHeader("Host", "api.nodester.com");
    	httpGet.setHeader("Authorization", "Basic " + Base64.encode(new String(uNamePass).getBytes()));
    	httpGet.setHeader("Content-Type", "application/json");
		
        try {
        	resp = mHttpClient.execute(httpGet);
		} catch (ClientProtocolException e1) {
			Log.e(TAG, "Error authenticating user Reason : " + e1.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e1.getMessage());
		} catch (IOException e2) {
			Log.e(TAG, "Error authenticating user Reason : " + e2.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e2.getMessage());
        }

        Log.i(TAG, "resp: " + resp);
        
        if(resp == null){
        	Log.e(TAG, "Server failed to respong. Response is null");
        	return false;
        }
        
        if ( resp != null){
        	final String response = EntityUtils.toString(resp.getEntity());
        	Log.i(TAG, " resp: " + response);
        	
        	int httpStatus = resp.getStatusLine().getStatusCode();
        	
        	if ( httpStatus == HttpStatus.SC_OK || httpStatus == HttpStatus.SC_CREATED){
        		Log.i(TAG, "Login successful");
        		
        		outputData.put( Constants.API_RESPONSE, new JSONArray(response) );
        		
        		return true;	
        	}
        	else
        	if ( httpStatus == HttpStatus.SC_UNAUTHORIZED ){
        		Log.e(TAG, "Login failed");
        		return false;
        	}
        }
        
        return false;
	}
    
    
    public boolean requestCoupon(String emailId, HashMap<String, String> outputData)  throws JSONException, ParseException, IOException, AuthenticationException{
    	
    	final String TAG = getClass().getName() + "requestCoupon";
    	
    	String apiUrl  = Constants.API_URL_COUPON;
        //String apiUrl  = Constants.API_URL_COUPON + "/?email=" + emailId;
        HttpResponse resp = null;

    	Log.i(TAG, "apiUrl: " + apiUrl);
    	
    	HttpGet httpGet = new HttpGet(apiUrl);
    	httpGet.setHeader("Accept", "application/json");
    	httpGet.setHeader("Host", "api.nodester.com");
    	httpGet.setHeader("Content-Type", "application/x-www-form-urlencoded");
    	BasicHttpParams params = new BasicHttpParams();
    	params.setParameter("email", emailId);
    	httpGet.setParams(params);
    	
    	JSONObject intouchIdJson = new JSONObject();
    	intouchIdJson.put("email", emailId);
    	
    	HttpPost httpPost = new HttpPost(apiUrl);
    	StringEntity jsonEntity = new StringEntity(intouchIdJson.toString());
    	jsonEntity.setContentEncoding( new BasicHeader(HTTP.CONTENT_TYPE, "application/json") );
    	httpPost.setEntity(jsonEntity);
    	
		
        try {
        	resp = mHttpClient.execute(httpGet);
		} catch (ClientProtocolException e1) {
			Log.e(TAG, "Error authenticating user Reason : " + e1.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e1.getMessage());
		} catch (IOException e2) {
			Log.e(TAG, "Error authenticating user Reason : " + e2.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e2.getMessage());
        }

        Log.i(TAG, "resp: " + resp);
        
        if(resp == null){
        	Log.e(TAG, "::requestCoupon Server failed to respong. Response is null");
        	return false;
        }
        
        if ( resp != null){
        	final String response = EntityUtils.toString(resp.getEntity());
        	Log.i(TAG, "::requestCoupon resp: " + response);
        	
        	int httpStatus = resp.getStatusLine().getStatusCode();
        	
        	if ( httpStatus == HttpStatus.SC_OK || httpStatus == HttpStatus.SC_CREATED){
        		Log.i(TAG, "Login successful");
        		JSONObject jsonObj = new JSONObject(response);
        		
        		String status = jsonObj.has(Constants.RESPONSE_JSON_STATUS) ? jsonObj.getString(Constants.RESPONSE_JSON_STATUS) : null;
        		
        		outputData.put( Constants.RESPONSE_JSON_STATUS, status ); 
        		
        		return true;	
        	}
        	else
        	if ( httpStatus == HttpStatus.SC_UNAUTHORIZED ){
        		Log.e(TAG, "::requestCoupon failed to autheticate.");
        		return false;
        	}
        }
        
        return false;
    }
    
     {
	  "status": "success - you are now in queue to receive an invite on our next batch!"
	}

    
    public boolean pingServer(String serverName)  throws JSONException, ParseException, IOException, AuthenticationException{
    	
    	String apiUrl  = "http://" + serverName + ".nodester.com/";
        HttpResponse resp = null;

    	Log.i(TAG, "apiUrl: " + apiUrl);
    	
    	HttpGet httpGet = new HttpGet(apiUrl);
		
        try {
        	resp = mHttpClient.execute(httpGet);
		} catch (ClientProtocolException e1) {
			Log.e(TAG, "#pingServer Error authenticating user Reason : " + e1.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e1.getMessage());
		} catch (IOException e2) {
			Log.e(TAG, "#pingServer Error authenticating user Reason : " + e2.getMessage());
			throw new AuthenticationException("Auth Failure Reason " + e2.getMessage());
        }

        Log.i(TAG, "resp: " + resp);
        
        if(resp == null){
        	Log.e(TAG, "#pingServer Server failed to respong. Response is null");
        	return false;
        }else{
        	final String response = EntityUtils.toString(resp.getEntity());
        	Log.i(TAG, "pingServer resp: " + response);
        	
        	int httpStatus = resp.getStatusLine().getStatusCode();
        	Log.i(TAG, "#pingServer httpStatus: " + httpStatus);
        	
        	if ( httpStatus == HttpStatus.SC_OK || httpStatus == HttpStatus.SC_CREATED){
        		Log.i(TAG, "Ping successful");
        		return true;	
        	}
        	else{
        		Log.e(TAG, "#pingServer failed to ping the server. Http status code: " + httpStatus);
        		return false;
        	}
        	
        }	// end else
    }
    */
  
}
